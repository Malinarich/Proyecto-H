
from django.contrib import admin
from django.urls import path, include
from PaginaWeb.views import MainPage

urlpatterns = [
    path('', MainPage),
]
